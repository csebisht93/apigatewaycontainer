﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.DataAccess.Constants
{

    public class DBFields
    {

        //-----------Common Base class field ---------------------------
        public static string Id = "id";
        public static string IsActive = "is_active";
        public static string CreatedBy = "created_by";
        public static string CreatedOn = "created_on";
        public static string ModifiedBy = "modified_by";
        public static string ModifiedOn = "modified_on";
        public static string CreatedIP = "ip_address";
        public static string ModifiedIP = "modified_ip";
        public static string CreatedMacAddress = "mac_address";
        public static string ModifiedMacAddress = "modified_mac";

        public static string ErrCode = "ErrCode";


        public static string CountryName = "country_name";
        public static string CountryId = "country_id";
        public static string StateName = "state_name";
        public static string StateId = "state_id";
        public static string CityName = "city_name";
        public static string CityId = "city_id";

        public static string PartnerIdNumber = "partner_id_number";
        public static string CompanyName = "company_name";
        public static string Address = "address";

        public static string Pincode = "pincode";
        public static string PartnerContactDetailXml = "PartnerContactDetailXml";
        public static string PartnerProductDetailXml = "PartnerProductDetailXml";

        public static string Title = "title";
        public static string DateOfBirth = "date_of_birth";
        public static string UserName = "user_name";
        public static string Password = "password";
        public static string FirstName = "first_name";
        public static string MiddleName = "middle_name";
        public static string LastName = "last_name";
        public static string ContactNo = "contact_no";
        public static string Email = "email";
        public static string DepartmentName = "department_name";
        public static string Designation = "designation";
        public static string ProductId = "product_id";
        public static string Variant = "Variant";
        public static string description = "description";

        public static string ProductName = "product_name";
        public static string CustomerSupportId = "support_id";
        public static string BranchName = "branch_name";
        public static string CompanyId = "company_id";
        public static string CustomerSuppEmployeelXml = "CustomerSuppEmployeelXml";
        public static string CustomerSuppProductXml = "CustomerSuppProductXml";


        public static string CompanyCode = "company_code";
        public static string Website = "website";
        public static string TinNo = "tin_no";
        public static string TanNo = "tan_no";
        public static string CinNo = "cin_no";
        public static string GstNo = "gst_no";
        public static string CompanyLogo = "company_logo";
        public static string BranchCode = "branch_code";
        public static string DepartmentCode = "department_code";

        public static string PartnerId = "partner_id";
        public static string cust_support_id = "cust_support_id";

        public static string EmployeeCode = "employee_code";
        public static string DepartmentId = "department_id";
        public static string BranchId = "branch_id";
        public static string DesignationId = "designation_id";
        public static string RoleId = "role_id";
        public static string FatherName = "father_name";
        public static string GenderId = "gender_id";
        public static string MaritalStatus = "marital_status";
        public static string Photograph = "photograph";

        public static string UserType = "user_type";
        public static string Salt = "salt";
        public static string LoginDateTime = "login_datetime";
        public static string LogOutDateTime = "logout_datetime";
        public static string UserId = "user_id";

        public static string RoleName = "role";
        public static string RoleDescription = "role_description";

        public static string VariantCode = "variant_code";
        public static string ProductTypeId = "product_type_id";


        public static string UsersAuditId = "UsersAuditId";

        public static string SessionId = "SessionId";
        public static string PageAccessed = "PageAccessed";
        public static string LoggedInAt = "LoggedInAt";
        public static string LoggedOutAt = "LoggedOutAt";
        public static string Method = "Method";
        public static string LoginStatus = "LoginStatus";
        public static string ControllerName = "ControllerName";
        public static string ActionName = "ActionName";
        public static string CustomerCount = "CustomerCount";
        public static string JsonData = "JsonData";
        public static string types = "type";



    }
    public class StoredProcedures
    {
        public static string CountryGet = "sp_CountryListGet";
        public static string StateGet = "sp_StateListGet";
        public static string CityGet = "sp_CityListGet";
        public static string InsertCountry = "sp_AddCountry";
        public static string InsertState = "sp_AddState";
        public static string InsertCity = "sp_AddCity";
        public static string InsertPartner = "sp_AddPartner";
        public static string GetPartnerList = "sp_GetPartnerList";
        public static string GetPartnerDetails = "sp_GetPartnerDetails";
        public static string GetPartnerContactList = "sp_GetPartnerContactList";
        public static string GetPartnerProductList = "sp_GetPartnerProductList";


        public static string InsertProduct = "sp_InsertProduct";
        public static string GetProductList = "sp_GetProductList";       public static string GetPartnerProduct = "sp_GetPartnerProduct";
        public static string GetProductById = "sp_GetProductById";public static string GetProductEditById = "sp_GetProductEditById";
        public static string GetCustomerSupportList = "sp_GetCustomerSupportList";
        public static string InsertCustomerSupport = "sp_InsertCustomerSupport";
        public static string GetCustomerSupportDetails = "sp_GetCustomerSupportDetails";
        public static string GetCustomerSupportEmployeeList = "sp_GetCustomerSupportEmployeeList";
        public static string GetCustomerSupportProductList = "sp_GetCustomerSupportProductList";

        public static string GetCompanyList = "sp_GetCompanyList";
        public static string InsertCompany = "sp_InsertCompany";
        public static string GetCompanyDetailsById = "sp_GetCompanyDetailsById";
        public static string GetBranchList = "sp_GetBranchList";
        public static string InsertBranch = "sp_InsertBranch";
        public static string GetBranchDetailsById = "sp_GetBranchDetailsById";
        public static string GetDepartmentList = "sp_GetDepartmentList";
        public static string InsertDepartment = "sp_InsertDepartment";
        public static string GetDepartmentDetailsById = "sp_GetDepartmentDetailsById";
        public static string GetDesignationList = "sp_GetDesignationList";
        public static string InsertDesignation = "sp_InsertDesignation";
        public static string GetDesignationDetailsById = "sp_GetDesignationDetailsById";

        public static string InsertPartnerContact = "sp_InsertPartnerContact";
        public static string InsertCustomerSupportEmployee = "sp_InsertCustomerSupportEmployee";

        public static string GetEmployeeList = "sp_GetEmployeeList";
        public static string InsertEmployee = "sp_InsertEmployee";
        public static string GetEmployeeDetailsById = "sp_GetEmployeeDetailsById";
        public static string GetUserDetails = "sp_GetUserDetails";


        public static string GetRoleList = "sp_GetRoleList";
        public static string InsertRole = "sp_InsertRole";
        public static string GetRoleDetailsById = "sp_GetRoleDetailsById";

        public static string InsertProductVariant = "sp_InsertProductVariant";
        public static string GetProductVariantList = "sp_GetProductVariantList";
        public static string GetProductVariantById = "sp_GetProductVariantById";


        public static string InsertAudit = "sp_InsertAudit";
        public static string ProductCustomerCount = "sp_ProductCustomerCount";
        public static string InactiveLoginAPIMethod = "sp_InactiveLoginAPIMethod";

    }
}
