﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class State:BaseClass
    {      
        public string Name { get; set; }
        public int CountryId { get; set; }
        public string country_name { get; set; }

    }
}
