﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class CustomerSupportProduct : BaseClass
    {
        public int CustomerSupportId { get; set; }

        public int ProductId { get; set; }

    }
}
