﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class Product : BaseClass
    {
        public string product_name { get; set; }
        public string product_id { get; set; }
        public string Variant { get; set; }
        public string description { get; set; }
    } 
    
}
