﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities.Baseclasses
{
    public class XMLCreater
    {
        public string GetPartnerProductXml(string Data)
        {
            string t_PartnerProductXml = "<NewDataSet>";
            if (Data == "")
            {
                return t_PartnerProductXml += "</NewDataSet>";
            }
            if (Data != null)
            {
                var PCList = JArray.Parse(Data);
                if (PCList.Count > 0)
                {
                    foreach (var item in PCList)
                    {
                        dynamic PCobj = JObject.Parse(item.ToString());
                        if (item.HasValues)
                        {

                            t_PartnerProductXml += "<cp_partner_product><product_id>"

                                    + PCobj.ProductId + "</product_id></cp_partner_product>";
                        }
                    }
                }

            }
            t_PartnerProductXml += "</NewDataSet>";
            return t_PartnerProductXml;
        }


        public string GetPartnerContactXml(string Data)
        {
            string t_PartnerContactXml = "<NewDataSet>";
            if (Data == "")
            {
                return t_PartnerContactXml += "</NewDataSet>";
            }
            if (Data != null)
            {
                var PCList = JArray.Parse(Data);
                if (PCList.Count > 0)
                {
                    foreach (var item in PCList)
                    {
                        dynamic PCobj = JObject.Parse(item.ToString());
                        if (item.HasValues)
                        {

                            t_PartnerContactXml += "<cp_partner_contact><user_name>"
                                 + PCobj.UserName + "</user_name><password>"
                                 + PCobj.Password + "</password><first_name>"
                                 + PCobj.FirstName + "</first_name><middle_name>"
                                 + PCobj.MiddleName + "</middle_name><last_name>"
                                 + PCobj.LastName + "</last_name><contact_no>"
                                 + PCobj.ContactNo + "</contact_no><email>"
                                 + PCobj.Email + "</email><department_name>"
                                 + PCobj.DepartmentName + "</department_name><designation>"
                                 + PCobj.Designation + "</designation></cp_partner_contact>";
                        }
                    }
                }

            }
            t_PartnerContactXml += "</NewDataSet>";
            return t_PartnerContactXml;
        }

        public string GetCustomerSuppEmployeeXml(string Data)
        {
            string t_CustSuppEmployeeXml = "<NewDataSet>";
            if (Data == "")
            {
                return t_CustSuppEmployeeXml += "</NewDataSet>";
            }
            if (Data != null)
            {
                var PCList = JArray.Parse(Data);
                if (PCList.Count > 0)
                {
                    foreach (var item in PCList)
                    {
                        dynamic PCobj = JObject.Parse(item.ToString());
                        if (item.HasValues)
                        {

                            t_CustSuppEmployeeXml += "<cp_support_employee><user_name>"
                                    + PCobj.UserName + "</user_name><password>"
                                    + PCobj.Password + "</password><first_name>"
                                    + PCobj.FirstName + "</first_name><middle_name>"
                                    + PCobj.MiddleName + "</middle_name><last_name>"
                                    + PCobj.LastName + "</last_name><contact_no>"
                                    + PCobj.ContactNo + "</contact_no><email>"
                                    + PCobj.Email + "</email><department_name>"
                                    + PCobj.DepartmentName + "</department_name><designation>"
                                    + PCobj.Designation + "</designation></cp_support_employee>";
                        }
                    }
                }

            }
            t_CustSuppEmployeeXml += "</NewDataSet>";
            return t_CustSuppEmployeeXml;
        }


        public string GetCustomerSuppProductXml(string Data)
        {
            string t_CustSuppProductXml = "<NewDataSet>";
            if (Data == "")
            {
                return t_CustSuppProductXml += "</NewDataSet>";
            }
            if (Data != null)
            {
                var PCList = JArray.Parse(Data);
                if (PCList.Count > 0)
                {
                    foreach (var item in PCList)
                    {
                        dynamic PCobj = JObject.Parse(item.ToString());
                        if (item.HasValues)
                        {

                            t_CustSuppProductXml += "<cp_support_product><product_id>"

                                    + PCobj.ProductId + "</product_id></cp_support_product>";
                        }
                    }
                }

            }
            t_CustSuppProductXml += "</NewDataSet>";
            return t_CustSuppProductXml;
        }

        public string GetProductVariantXml(string Data)
        {
            string t_CustSuppProductXml = "<NewDataSet>";
            if (Data == "[]")
            {
                return "";///t_CustSuppProductXml += "</NewDataSet>";
            }
            if (Data != null)
            {
                var PCList = JArray.Parse(Data);
                if (PCList.Count > 0)
                {
                    foreach (var item in PCList)
                    {
                        dynamic PCobj = JObject.Parse(item.ToString());
                        if (item.HasValues)
                        {

                            t_CustSuppProductXml += "<cp_product_variant><Variantcode>"
                                 + PCobj.Variantcode + "</Variantcode><VariantName>"
                                 + PCobj.VariantName + "</VariantName><UIType>"
                                 + PCobj.UIType + "</UIType><VariantDescription>"
                                 + PCobj.VariantDescription + "</VariantDescription></cp_product_variant>";
                        }
                    }
                }

            }
            t_CustSuppProductXml += "</NewDataSet>";
            return t_CustSuppProductXml;
        }
    }

       
}

