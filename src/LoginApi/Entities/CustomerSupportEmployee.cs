﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class CustomerSupportEmployee : BaseClass
    {
        public int CustomerSupportId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string DepartmentName { get; set; }
        public string Designation { get; set; }
        public int UserId { get; set; }


    }

}
