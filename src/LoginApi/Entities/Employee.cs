﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class Employee : BaseClass
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EmployeeCode { get; set; }
        public int CompanyId { get; set; }
        public int DepartmentId { get; set; }
        public int BranchId { get; set; }
        public int DesignationId { get; set; }
        public int RoleId { get; set; }
        public string FatherName { get; set; }
        public int GenderId { get; set; }
        public string DateOfBirth { get; set; }
        public int MaritalStatus { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string Address { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string Pincode { get; set; }
        public string Photograph { get; set; }
        public int UserId { get; set; }

    }

    
}
