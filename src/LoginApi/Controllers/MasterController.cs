﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoginApi.Business;
using LoginApi.Entities;
using LoginApi.Entities.Baseclasses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace LoginApi.Controllers
{
    [Route("Login-Api/[controller]/[action]")]
    [ApiController]
    public class MasterController : ControllerBase
    {
        BusinessFacade objBusinessFacade = new BusinessFacade();
         ILoggerFactory _loggerFactory;
        [HttpGet]
        public List<Country> GetCountryList()
        {
            List<Country> countryList = new List<Country>();
            try
            {             
                countryList= objBusinessFacade.GetCountry();
            }
            catch(Exception ex) 
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                countryList = new List<Country>();
            }
            finally 
            {
                // countryList =new List<Country>();
            }
            return countryList;
        }


        [HttpGet]
        public List<State> GetStateList()
        {
            List<State> StateList = new List<State>();
            try
            {
                StateList = objBusinessFacade.GetState();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
              //  countryList = new List<Country>();
            }
            return StateList;
        }

        [HttpGet]
        public List<City> GetCityList()
        {
            List<City> countryList = new List<City>();
            try
            {
                countryList = objBusinessFacade.GetCity();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
             //   countryList = new List<Country>();
            }
            return countryList;
        }

        #region insert country
        public ServiceResponse InsertCountry(Country pobjCountry)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {                
                response = objBusinessFacade.InsertCountry(pobjCountry);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert Country M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region insert state
        public ServiceResponse InsertState(State pobjState)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertState(pobjState);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert State M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }

        #endregion

        #region insert city
        public ServiceResponse InsertCity(City pobjCity)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertCity(pobjCity);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert City M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region insert Partner
        public ServiceResponse InsertPartner(Partner pobjPartner)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertPartner(pobjPartner);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert Parner M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region Partner List 
        [HttpGet]
        public List<Partner> GetPartnerList()
        {
            List<Partner> PartnerList = new List<Partner>();
            try
            {
                PartnerList = objBusinessFacade.GetPartnerList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return PartnerList;
        }
        #endregion

        //for Partner Edit
        #region Partner Details 
        [HttpGet]
        public List<Partner> GetPartnerDetails(int? PartnerId)
        {
            List<Partner> PartnerList = new List<Partner>();
            try
            {
                PartnerList = objBusinessFacade.GetPartnerDetails(PartnerId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return PartnerList;
        }
        #endregion


        #region Partner ContactList 
        [HttpGet]
        public List<PartnerContactDetails> GetPartnerContactList(int? PartnerId)
        {
            List<PartnerContactDetails> PartnerList = new List<PartnerContactDetails>();
            try
            {
                PartnerList = objBusinessFacade.GetPartnerContactList(PartnerId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return PartnerList;
        }
        #endregion

        #region Partner ProductList 
        [HttpGet]
        public List<PartnerProductDetails> GetPartnerProductList(int? PartnerId)
        {
            List<PartnerProductDetails> PartnerList = new List<PartnerProductDetails>();
            try
            {
                PartnerList = objBusinessFacade.GetPartnerProductList(PartnerId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return PartnerList;
        }
        #endregion

        #region insert Product
        public ServiceResponse InsertProduct(Product pobjProduct)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertProduct(pobjProduct);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region ProductList 
        [HttpGet]
        public List<Product> GetProductList()
        {
            List<Product> PartnerList = new List<Product>();
            try
            {
                PartnerList = objBusinessFacade.GetProductList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return PartnerList;
        }
        #endregion

        #region Edit Product
        [HttpGet]
        public List<Product> GetProductById(int? ProductId)
        {
            List<Product> ProductList = new List<Product>();
            try
            {
                ProductList = objBusinessFacade.GetProductById(ProductId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return ProductList;
        }
        #endregion

        #region Customer SupportList 
        [HttpGet]
        public List<CustomerSupport> GetCustomerSupportList()
        {
            List<CustomerSupport> CustList = new List<CustomerSupport>();
            try
            {
                CustList = objBusinessFacade.GetCustomerSupportList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return CustList;
        }
        #endregion


        #region insert Customer Support
        public ServiceResponse InsertCustomerSupport(CustomerSupport pobjCustomerSupport)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertCustomerSupport(pobjCustomerSupport);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert Parner M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion



        //for Customer Support Edit

        #region Customer Support Details 
        [HttpGet]
        public List<CustomerSupport> GetCustomerSupportDetails(int? CustomerSupportId)
        {
            List<CustomerSupport> CustSuppList = new List<CustomerSupport>();
            try
            {
                CustSuppList = objBusinessFacade.GetCustomerSupportDetails(CustomerSupportId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return CustSuppList;
        }
        #endregion


        #region Support Employee List
        [HttpGet]
        public List<CustomerSupportEmployee> GetCustomerSupportEmployee(int? CustomerSupportId)
        {
            List<CustomerSupportEmployee> SupportEmployeeList = new List<CustomerSupportEmployee>();
            try
            {
                SupportEmployeeList = objBusinessFacade.GetCustomerSupportEmployee(CustomerSupportId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return SupportEmployeeList;
        }
        #endregion

        #region Support Employee ProductList 
        [HttpGet]
        public List<CustomerSupportProduct> GetCustomerSupportProductList(int? CustomerSupportId)
        {
            List<CustomerSupportProduct> PartnerList = new List<CustomerSupportProduct>();
            try
            {
                PartnerList = objBusinessFacade.GetCustomerSupportProductList(CustomerSupportId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return PartnerList;
        }
        #endregion
        #region Company section

        #region Companylist
        [HttpGet]
        public List<Company> GetCompanyList()
        {
            List<Company> objCustomerList = new List<Company>();
            try
            {
                objCustomerList = objBusinessFacade.GetCompanyList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objCustomerList;
        }
        #endregion


        #region insert Company
        public ServiceResponse InsertCompany(Company pobjCompany)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertCompany(pobjCompany);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert Company M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region Company Edit
        [HttpGet]
        public List<Company> GetCompanyDetailsById(int? CompanyId)
        {
            List<Company> objCompany = new List<Company>();
            try
            {
                objCompany = objBusinessFacade.GetCompanyDetailsById(CompanyId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objCompany;
        }
        #endregion

        #endregion

        #region Branch section
        #region Branch list
        [HttpGet]
        public List<Branch> GetBranchList()
        {
            List<Branch> objBranch = new List<Branch>();
            try
            {
                objBranch = objBusinessFacade.GetBranchList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objBranch;
        }
        #endregion


        #region insert Branch
        public ServiceResponse InsertBranch(Branch pobjBranch)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertBranch(pobjBranch);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert Company M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region Branch Edit
        [HttpGet]
        public List<Branch> GetBranchDetailsById(int? BranchId)
        {
            List<Branch> objBranch = new List<Branch>();
            try
            {
                objBranch = objBusinessFacade.GetBranchDetailsById(BranchId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objBranch;
        }
        #endregion

        #endregion
        #region Department section
        #region Department list
        [HttpGet]
        public List<Department> GetDepartmentList()
        {
            List<Department> objDepartment = new List<Department>();
            try
            {
                objDepartment = objBusinessFacade.GetDepartmentList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objDepartment;
        }
        #endregion


        #region insert Department
        public ServiceResponse InsertDepartment(Department pobjDepartment)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertDepartment(pobjDepartment);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert Dept M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region Department Edit
        [HttpGet]
        public List<Department> GetDepartmentDetailsById(int? DepartmentId)
        {
            List<Department> objBranch = new List<Department>();
            try
            {
                objBranch = objBusinessFacade.GetDepartmentDetailsById(DepartmentId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objBranch;
        }
        #endregion

        #endregion

        #region Designation section
        #region Designation list
        [HttpGet]
        public List<Designation> GetDesignationList()
        {
            List<Designation> objDesignation = new List<Designation>();
            try
            {
                objDesignation = objBusinessFacade.GetDesignationList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objDesignation;
        }
        #endregion



        #region insert Designation
        public ServiceResponse InsertDesignation(Designation pobjDesignation)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertDesignation(pobjDesignation);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert Dept M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region Designation Edit
        [HttpGet]
        public List<Designation> GetDesignationDetailsById(int? DesignationId)
        {
            List<Designation> objDesignation = new List<Designation>();
            try
            {
                objDesignation = objBusinessFacade.GetDesignationDetailsById(DesignationId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objDesignation;
        }
        #endregion

        #endregion

        #region Partner ContactDetails        
        public ServiceResponse InsertPartnerContact(PartnerContactDetails pobjPartnerContact)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertPartnerContact(pobjPartnerContact);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert PartnerContact M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }

        #endregion
        #region Partner ContactDetails        
        public ServiceResponse InsertSupportEmployee(CustomerSupportEmployee pobjPartnerContact)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertCustomerSupportEmployee(pobjPartnerContact);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert PartnerContact M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }

        #endregion

        #region Employee section

        #region Employee list
        [HttpGet]
        public List<Employee> GetEmployeeList()
        {
            List<Employee> objEmployeeList = new List<Employee>();
            try
            {
                objEmployeeList = objBusinessFacade.GetEmployeeList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objEmployeeList;
        }
        #endregion


        #region insert Employee
        public ServiceResponse InsertEmployee(Employee pobjEmployee)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertEmployee(pobjEmployee);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert Employee M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region Employee Edit
        [HttpGet]
        public List<Employee> GetEmployeeDetailsById(int? EmployeeId)
        {
            List<Employee> objEmployee = new List<Employee>();
            try
            {
                objEmployee = objBusinessFacade.GetEmployeeDetailsById(EmployeeId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objEmployee;
        }
        #endregion

        #endregion


        #region Login
        [HttpPost]
        public Response<UserLogin> GetUserDetails(UserLogin objUser)
        {
            List<UserLogin> objUserLogin = new List<UserLogin>();
            Response<UserLogin> response = new Response<UserLogin>();
            try
            {
               var List = objBusinessFacade.GetUserDetails(objUser);

                if (List.Count > 0)
                {
                    //response.ListObject = List;
                    //response.Count = List.Count;
                    //response.IsSuccess = true;                 
                    //response.Message = "Data Found";
                    //response.Error = "200";

                    var getpassword = Encryptdecrypt.DecryptString(List.FirstOrDefault().Password, "");
                    if (getpassword== objUser.Password) 
                    {
                        response.ListObject = List;
                        response.Count = List.Count;
                        response.IsSuccess = true;
                        response.Message = "password Match";
                        response.Error = "200";
                    }
                    else 
                    {
                        response.ListObject =new List<UserLogin>();
                        response.Count = 0;
                        response.IsSuccess = false;
                        response.Message = "Wrong password";
                        response.Error = "404";
                    }
                }
                else 
                {
                    response.ListObject = new List<UserLogin>();
                    response.Count = 0;
                    response.IsSuccess = false;                  
                    response.Message = "No Data Found";
                    response.Error = "404";
                }
            }
            catch (Exception ex)
            {
                response.ListObject = new List<UserLogin>();
                response.IsSuccess = false;
                response.Count = 0;
                response.Message = "Something Went Wrong";
                response.Error = "500";
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return response;
        }

        #endregion

        #region Role section
        #region Role list
        [HttpGet]
        public List<Role> GetRoleList()
        {
            List<Role> objRole = new List<Role>();
            try
            {
                objRole = objBusinessFacade.GetRoleList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objRole;
        }
        #endregion



        #region insert Role
        public ServiceResponse InsertRole(Role pobjRole)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertRole(pobjRole);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert Role M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region Role Edit
        [HttpGet]
        public List<Role> GetRoleDetailsById(int? RoleId)
        {
            List<Role> objRole = new List<Role>();
            try
            {
                objRole = objBusinessFacade.GetRoleDetailsById(RoleId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objRole;
        }
        #endregion

        #endregion


        #region ProductVariant

        #region insert ProductVariant
        public ServiceResponse InsertProductVariant(ProductVariant pobjProduct)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objBusinessFacade.InsertProductVariant(pobjProduct);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region ProductVariantList 
        [HttpGet]
        public List<ProductVariant> GetProductVariantList()
        {
            List<ProductVariant> ProductVariantList = new List<ProductVariant>();
            try
            {
                ProductVariantList = objBusinessFacade.GetProductVariantList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return ProductVariantList;
        }
        #endregion
        #region ProductVariantList 
        [HttpGet]
        public List<ProductVariant> GetProductVariantListForProduct()
        {
            List<ProductVariant> ProductVariantList = new List<ProductVariant>();
            try
            {
                ProductVariantList = objBusinessFacade.GetProductVariantListForProduct();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return ProductVariantList;
        }
        #endregion

        #region Edit ProductVariant
        [HttpGet]
        public List<ProductVariant> GetProductVariantById(int? Id)
        {
            List<ProductVariant> ProductVariantList = new List<ProductVariant>();
            try
            {
                ProductVariantList = objBusinessFacade.GetProductVariantById(Id);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return ProductVariantList;
        }
        #endregion

        #endregion

        #region ProductList 
        [HttpGet]
        public List<Product> GetPartnerProduct(int id)
        {
            List<Product> PartnerList = new List<Product>();
            try
            {
                PartnerList = objBusinessFacade.GetPartnerProduct(id);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return PartnerList;
        }
        #endregion
       
        #region Login
        [HttpPost]
        public Response<UserLogin> GetCommunicationUserDetails(UserLogin objUser)
        {
            List<UserLogin> objUserLogin = new List<UserLogin>();
            Response<UserLogin> response = new Response<UserLogin>();
           // objUser.UserName = Encryptdecrypt.DecryptString(objUser.UserName,"");
          //  objUser.Password = Encryptdecrypt.DecryptString(objUser.Password.Replace(" ","+"), "");
            objUser.Password = Encryptdecrypt.DecryptString(objUser.Password, "");
            try
            {
                var List = objBusinessFacade.GetUserDetails(objUser);

                if (List.Count > 0)
                {
                    //response.ListObject = List;
                    //response.Count = List.Count;
                    //response.IsSuccess = true;                 
                    //response.Message = "Data Found";
                    //response.Error = "200";

                    var getpassword = Encryptdecrypt.DecryptString(List.FirstOrDefault().Password, "");
                    if (getpassword == objUser.Password)
                    {
                        response.ListObject = List;
                        response.Count = List.Count;
                        response.IsSuccess = true;
                        response.Message = "password Match";
                        response.Error = "200";
                    }
                    else
                    {
                        response.ListObject = new List<UserLogin>();
                        response.Count = 0;
                        response.IsSuccess = false;
                        response.Message = "Wrong password";
                        response.Error = "404";
                    }
                }
                else
                {
                    response.ListObject = new List<UserLogin>();
                    response.Count = 0;
                    response.IsSuccess = false;
                    response.Message = "No Data Found";
                    response.Error = "404";
                }
            }
            catch (Exception ex)
            {
                response.ListObject = new List<UserLogin>();
                response.IsSuccess = false;
                response.Count = 0;
                response.Message = "Something Went Wrong";
                response.Error = "500";
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return response;
        }

        #endregion

        #region InactiveLoginAPIMethod
        [HttpGet]
        public Response<dynamic> InactiveLoginAPIMethod(int PrimaryKey,int type,int UserId)
        {
          
            Response<dynamic> response = new Response<dynamic>();                   
            try
            {
                response.Message = objBusinessFacade.InactiveLoginAPIMethod(PrimaryKey, type, UserId);
                response.Error = "200";



            }
            catch (Exception ex)
            {
                //response.ListObject = new List<UserLogin>();
                //response.IsSuccess = false;
                //response.Count = 0;
                //response.Message = "Something Went Wrong";
                //response.Error = "500";
                //var logger = _loggerFactory.CreateLogger("LoginCategory");
                //logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return response;
        }

        #endregion

    }
}