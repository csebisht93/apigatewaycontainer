﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoginApi.Business;
using LoginApi.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LoginApi.Controllers
{
    [Route("Login-Api/[controller]/[action]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        BusinessFacade objBusinessFacade = new BusinessFacade();

        [HttpGet]
        public List<ReportCarrier> GetProductCustomerCount()
        {
            List<ReportCarrier> countryList = new List<ReportCarrier>();
            try
            {
                countryList = objBusinessFacade.GetProductCustomerCount();
            }
            catch (Exception ex)
            {
                
                
                countryList = new List<ReportCarrier>();
            }
            finally
            {
                // countryList =new List<Country>();
            }
            return countryList;
        }
    }
}