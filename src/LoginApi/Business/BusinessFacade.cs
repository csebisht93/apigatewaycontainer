﻿using LoginApi.DataAccess.Mapper;
using LoginApi.DataAccess.Wrapper;
using LoginApi.Entities;
using LoginApi.Entities.Baseclasses;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Business
{
    public class BusinessFacade
    {

        #region Get Country List
        ILoggerFactory _loggerFactory;
        public List<Country> GetCountry()
        {
            List<Country> list = new List<Country>();
            try
            {
                list = DataWrapper.MapGetCountry(SPWrapper.GetCountry());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
              
            }

            return list;
        }
        #endregion
        #region Get State List

        public List<State> GetState()
        {
            List<State> list = new List<State>(); 
            try
            {
                list = DataWrapper.MapGetState(SPWrapper.GetState());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Get State List

        public List<City> GetCity()
        {
            List<City> list = new List<City>();
            try
            {
                list = DataWrapper.MapGetCity(SPWrapper.GetCity());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion


        #region country save
        public ServiceResponse InsertCountry(Country pobjCountry)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertCountry(pobjCountry));
        }
        #endregion

        #region State save
        public ServiceResponse InsertState(State pobjSatte)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertState(pobjSatte));
        }
        #endregion

        #region City save
        public ServiceResponse InsertCity(City pobjCity)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertCity(pobjCity));
        }
        #endregion

        #region Partner save
        public ServiceResponse InsertPartner(Partner pobjPartner)
        {
            return DataWrapper.PartnerMapResponse(SPWrapper.InsertPartner(pobjPartner));
        }
        #endregion

        #region for edit
        public List<Partner> GetPartnerDetails(int? PartnerId)
        {
            List<Partner> list = null;
            try
            {
                list = DataWrapper.MapGetPartnerDetails(SPWrapper.GetPartnerDetails(PartnerId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region partner contact List
        public List<PartnerContactDetails> GetPartnerContactList(int? PartnerId)
        {
            List<PartnerContactDetails> list = new List<PartnerContactDetails>();
            try
            {
                list = DataWrapper.MapGetPartnerContactList(SPWrapper.GetPartnerContactList(PartnerId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region partner product List
        public List<PartnerProductDetails> GetPartnerProductList(int? PartnerId)
        {
            List<PartnerProductDetails> list = new List<PartnerProductDetails>();
            try
            {
                list = DataWrapper.MapGetPartnerProductList(SPWrapper.GetPartnerProductList(PartnerId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region product save
        public ServiceResponse InsertProduct(Product pobjProduct)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertProduct(pobjProduct));
        }
        #endregion


        #region product List
        public List<Product> GetProductList()
        {
            List<Product> list = new List<Product>();
            try
            {
                list = DataWrapper.MapGetProductList(SPWrapper.GetProductList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion 
        #region product List
        public List<Product> GetPartnerProduct(int id)
        {
            List<Product> list = new List<Product>();
            try
            {
                list = DataWrapper.MapGetProductList(SPWrapper.GetPartnerProduct(id));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Edit Product 
        public List<Product> GetProductById(int? ProductId)
        {
            List<Product> list = null;
            try
            {
                list = DataWrapper.MapGetProductById(SPWrapper.GetProductById(ProductId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region CusrtomerSupp List
        public List<CustomerSupport> GetCustomerSupportList()
        {
            List<CustomerSupport> list = new List<CustomerSupport>();
            try
            {
                list = DataWrapper.MapGetCustomerSupportList(SPWrapper.GetCustomerSupportList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region CustomerSupport save
        public ServiceResponse InsertCustomerSupport(CustomerSupport pobjCustomerSupport)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertCustomerSupport(pobjCustomerSupport));
        }
        #endregion


        //for Customer Support Edit
        #region Customer Support Edit
        public List<CustomerSupport> GetCustomerSupportDetails(int? CustomerSupportId)
        {
            List<CustomerSupport> list = null;
            try
            {
                list = DataWrapper.MapGetCustomerSupportDetails(SPWrapper.GetCustomerSupportDetails(CustomerSupportId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion


        #region Support Employee List
        public List<CustomerSupportEmployee> GetCustomerSupportEmployee(int? CustomerSupportId)
        {
            List<CustomerSupportEmployee> list = new List<CustomerSupportEmployee>(); ;
            try
            {
                list = DataWrapper.MapGetCustomerSupportEmployeeList(SPWrapper.GetCustomerSupportEmployeeList(CustomerSupportId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Support Employee ProductList
        public List<CustomerSupportProduct> GetCustomerSupportProductList(int? CustomerSupportId)
        {
            List<CustomerSupportProduct> list = new List<CustomerSupportProduct>();
            try
            {
                list = DataWrapper.MapGetCustomerSupportProductList(SPWrapper.GetCustomerSupportProductList(CustomerSupportId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        public List<Partner> GetPartnerList()
        {
            List<Partner> list = new List<Partner>(); 
            try
            {
                list = DataWrapper.MapGetPartnerList(SPWrapper.GetPartnerList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #region Company section

        #region Company list
        public List<Company> GetCompanyList()
        {
            List<Company> list = new List<Company>(); ;
            try
            {
                list = DataWrapper.MapGetCompanyList(SPWrapper.GetCompanyList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Company save
        public ServiceResponse InsertCompany(Company pobjCompany)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertCompany(pobjCompany));
        }
        #endregion

        #region Company Edit
        public List<Company> GetCompanyDetailsById(int? CompanyId)
        {
            List<Company> list = null;
            try
            {
                list = DataWrapper.MapGetCompanyDetailsById(SPWrapper.GetCompanyDetailsById(CompanyId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #endregion

        #region Branch section
        #region Branch list
        public List<Branch> GetBranchList()
        {
            List<Branch> list = new List<Branch>();
            try
            {
                list = DataWrapper.MapGetBranchList(SPWrapper.GetBranchList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Branch save
        public ServiceResponse InsertBranch(Branch pobjBranch)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertBranch(pobjBranch));
        }
        #endregion

        #region Branch Edit
        public List<Branch> GetBranchDetailsById(int? BranchId)
        {
            List<Branch> list = null;
            try
            {
                list = DataWrapper.MapGetBranchDetailsById(SPWrapper.GetBranchDetailsById(BranchId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #endregion

        #region Department section
        #region Department list
        public List<Department> GetDepartmentList()
        {
            List<Department> list = new List<Department>();
            try
            {
                list = DataWrapper.MapGetDepartmentList(SPWrapper.GetDepartmentList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Department save
        public ServiceResponse InsertDepartment(Department pobjDepartment)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertDepartment(pobjDepartment));
        }
        #endregion

        #region Department Edit
        public List<Department> GetDepartmentDetailsById(int? DepartmentId)
        {
            List<Department> list = null;
            try
            {
                list = DataWrapper.MapGetDepartmentDetailsById(SPWrapper.GetDepartmentDetailsById(DepartmentId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #endregion

        #region Designation section
        #region Designation list
        public List<Designation> GetDesignationList()
        {
            List<Designation> list = new List<Designation>();
            try
            {
                list = DataWrapper.MapGetDesignationList(SPWrapper.GetDesignationList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Designation save
        public ServiceResponse InsertDesignation(Designation pobjDesignation)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertDesignation(pobjDesignation));
        }
        #endregion

        #region Designation Edit
        public List<Designation> GetDesignationDetailsById(int? DesignationId)
        {
            List<Designation> list = null;
            try
            {
                list = DataWrapper.MapGetDesignationDetailsById(SPWrapper.GetDesignationDetailsById(DesignationId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #endregion
        #region Company save
        public ServiceResponse InsertPartnerContact(PartnerContactDetails pobjPartnerContact)
        {
            return DataWrapper.PartnerMapResponse(SPWrapper.InsertPartnerContact(pobjPartnerContact));
        }
        #endregion  
        #region Company save
        public ServiceResponse InsertCustomerSupportEmployee(CustomerSupportEmployee pobjPartnerContact)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertCustomerSupportEmployee(pobjPartnerContact));
        }
        #endregion

        #region Employee section

        #region Employee list
        public List<Employee> GetEmployeeList()
        {
            List<Employee> list = new List<Employee>();
            try
            {
                list = DataWrapper.MapGetEmployeeList(SPWrapper.GetEmployeeList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Employee save
        public ServiceResponse InsertEmployee(Employee pobjEmployee)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertEmployee(pobjEmployee));
        }
        #endregion

        #region Employee Edit
        public List<Employee> GetEmployeeDetailsById(int? EmployeeId)
        {
            List<Employee> list = null;
            try
            {
                list = DataWrapper.MapGetEmployeeDetailsById(SPWrapper.GetEmployeeDetailsById(EmployeeId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #endregion


        #region Login
        public List<UserLogin> GetUserDetails(UserLogin objUser)
        {
            List<UserLogin> list = new List<UserLogin>();
            try
            {
                list = DataWrapper.MapGetUserDetails(SPWrapper.GetUserDetails(objUser));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Role section
        #region Role list
        public List<Role> GetRoleList()
        {
            List<Role> list = new List<Role>();
            try
            {
                list = DataWrapper.MapGetRoleList(SPWrapper.GetRoleList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Role save
        public ServiceResponse InsertRole(Role pobjRole)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertRole(pobjRole));
        }
        #endregion

        #region Role Edit
        public List<Role> GetRoleDetailsById(int? RoleId)
        {
            List<Role> list = null;
            try
            {
                list = DataWrapper.MapGetRoleDetailsById(SPWrapper.GetRoleDetailsById(RoleId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #endregion

        #region ProductVariant

        #region ProductVariant save
        public ServiceResponse InsertProductVariant(ProductVariant pobjProduct)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertProductVariant(pobjProduct));
        }
        #endregion

        #region ProductVariant List
        public List<ProductVariant> GetProductVariantList()
        {
            List<ProductVariant> list = new List<ProductVariant>();
            try
            {
                list = DataWrapper.MapGetProductListNew(SPWrapper.GetProductList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion
        #region ProductVariant List
        public List<ProductVariant> GetProductVariantListForProduct()
        {
            List<ProductVariant> list = new List<ProductVariant>();
            try
            {
                list = DataWrapper.MapGetProductVariantList(SPWrapper.GetProductVariantList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion
        #region Edit ProductVariant 
        public List<ProductVariant> GetProductVariantById(int? Id)
        {
            List<ProductVariant> list = null;
            try
            {
                list = DataWrapper.MapGetProductVariantById(SPWrapper.GetProductVariantById(Id));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #endregion

        #region Partner save
        public Response<AuditTb> InsertAuditTb(AuditTb pobjAuditTb)
        {
            return DataWrapper.InsertAuditResponse(SPWrapper.InsertAuditTb(pobjAuditTb));
        }
        #endregion
        public List<ReportCarrier> GetProductCustomerCount()
        {
            List<ReportCarrier> list = new List<ReportCarrier>();
            try
            {
                list = DataWrapper.MapProductCustomerCount(SPWrapper.GetProductCustomerCount());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }


        #region InactiveLoginAPIMethod
        public dynamic InactiveLoginAPIMethod(int PrimaryKey, int type, int UserId)
        {
            dynamic list = "";
            try
            {
                list = DataWrapper.InactiveLoginAPIMethod(SPWrapper.InactiveLoginAPIMethod(PrimaryKey,type, UserId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion
    }
}
