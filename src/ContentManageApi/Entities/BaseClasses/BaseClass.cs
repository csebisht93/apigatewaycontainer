﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentManageApi.Entities.BaseClasses
{
    public class BaseClass
    {
        public int Id { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string CreatedIP { get; set; }
        public string ModifiedIP { get; set; }
        public string CreatedMacAddress { get; set; }
        public string ModifiedMacAddress { get; set; }
        public bool IsActive { get; set; }
    }
}
