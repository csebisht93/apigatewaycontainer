﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ContentManageApi.Entities;
using ContentManageApi.Business;

namespace ContentManageApi.Controllers
{
    [Route("Content-Api/[controller]/[action]")]
    [ApiController]
    public class ContentManageController : ControllerBase
    {

        ContentFacade objContentFacade = new ContentFacade();
        ILoggerFactory _loggerFactory;

        [HttpGet]
        public List<CommonClass> GetContactDetails()
        {
            List<CommonClass> objList = new List<CommonClass>();
            try
            {
                objList = objContentFacade.GetContactDetails();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                objList = new List<CommonClass>();
            }
            finally
            {
             
            }
            return objList;
        }


        [HttpGet]
        public List<CommonClass> GetHomePageDetails()
        {
            List<CommonClass> objList = new List<CommonClass>();
            try
            {
                objList = objContentFacade.GetHomePageDetails();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                objList = new List<CommonClass>();
            }
            finally
            {

            }
            return objList;
        }

        [HttpGet]
        public List<CommonClass> GetInfoPageDetails()
        {
            List<CommonClass> objList = new List<CommonClass>();
            try
            {
                objList = objContentFacade.GetInfoPageDetails();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                objList = new List<CommonClass>();
            }
            finally
            {

            }
            return objList;
        }

        [HttpGet]
        public List<CommonClass> GetPlanPageDetails()
        {
            List<CommonClass> objList = new List<CommonClass>();
            try
            {
                objList = objContentFacade.GetPlanPageDetails();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                objList = new List<CommonClass>();
            }
            finally
            {

            }
            return objList;
        }


        [HttpGet]
        public List<CommonClass> GetFAQPageDetails()
        {
            List<CommonClass> objList = new List<CommonClass>();
            try
            {
                objList = objContentFacade.GetFAQPageDetails();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                objList = new List<CommonClass>();
            }
            finally
            {

            }
            return objList;
        }

        [HttpGet]
        public List<CommonClass> GetFooterPageDetails()
        {
            List<CommonClass> objList = new List<CommonClass>();
            try
            {
                objList = objContentFacade.GetFooterPageDetails();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                objList = new List<CommonClass>();
            }
            finally
            {

            }
            return objList;
        }


        [HttpGet]
        public List<CommonClass> GetHeaderPageDetails()
        {
            List<CommonClass> objList = new List<CommonClass>();
            try
            {
                objList = objContentFacade.GetHeaderPageDetails();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                objList = new List<CommonClass>();
            }
            finally
            {

            }
            return objList;
        }





    }
}
