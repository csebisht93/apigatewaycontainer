﻿using Microsoft.Extensions.Logging;
using CommunicationApi.DataAccess.Mapper;
using CommunicationApi.DataAccess.Wrapper;
using CommunicationApi.Entities;
using CommunicationApi.Entities.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationApi.Business
{
    public class SmsEngineFacade
    {
        ILoggerFactory _loggerFactory;


        #region SmsTemplate section
        #region SmsTemplate list
        public List<SmsTemplate> GetSmsTemplateList()
        {
            List<SmsTemplate> list = new List<SmsTemplate>();
            try
            {
                list = DataWrapper.MapGetSmsTemplateList(SPWrapper.GetSmsTemplateList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region SmsTemplate save
        public ServiceResponse InsertSmsTemplate(SmsTemplate pobjSmsTemplate)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertSmsTemplate(pobjSmsTemplate));
        }
        #endregion

        #region SmsTemplate Edit
        public List<SmsTemplate> GetSmsTemplateDetailsById(int? SmsTemplateId)
        {
            List<SmsTemplate> list = null;
            try
            {
                list = DataWrapper.MapGetSmsTemplateDetailsById(SPWrapper.GetSmsTemplateDetailsById(SmsTemplateId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion
        #endregion


    #region SmsConfig section
        #region SmsConfig list
        public List<SmsConfig> GetSmsConfigList()
        {
            List<SmsConfig> list = new List<SmsConfig>();
            try
            {
                list = DataWrapper.MapGetSmsConfigList(SPWrapper.GetSmsConfigList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region SmsConfig save
        public ServiceResponse InsertSmsConfig(SmsConfig pobjSmsConfig)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertSmsConfig(pobjSmsConfig));
        }
        #endregion

        #region SmsConfig Edit
        public List<SmsConfig> GetSmsConfigDetailsById(int? SmsConfigId)
        {
            List<SmsConfig> list = null;
            try
            {
                list = DataWrapper.MapGetSmsConfigDetailsById(SPWrapper.GetSmsConfigDetailsById(SmsConfigId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion
 #endregion


    }
}
