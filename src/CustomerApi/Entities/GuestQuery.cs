﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UploadEmployeeApi.Entities
{
    public class GuestQuery : BaseClass
    {
        public string name { get; set; }
        public string mobile_number { get; set; }
        public string email { get; set; }
        public string question { get; set; }
        public string Ago { get; set; }
    }
}
