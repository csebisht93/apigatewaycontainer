﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UploadEmployeeApi.Entities.BaseClasses;

namespace UploadEmployeeApi.Entities
{
    public class OTPEntry:BaseClass
    {
        public string Msg { get; set; }
        public string MobileNumber { get; set; }
        public bool Status { get; set; }
        public string MemberShipNo { get; set; }
        public string ConfigId { get; set; }
        public string TemplateCode { get; set; }
        public string TemplateId { get; set; }
        public int OTP { get; set; }
        public string OtpType { get; set; }
    }
}
