﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UploadEmployeeApi.Entities.BaseClasses;

namespace UploadEmployeeApi.Entities
{
    public class Customer : BaseClass
    {
        public string Title { get; set; }
        public string Name { get; set; }
        public string MembershipNo { get; set; }
        public string ContactNo { get; set; }
        public string AlternateContactNo { get; set; }
        public string Email { get; set; }
        public string AlternateEmail { get; set; }
        public string DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Pincode { get; set; }
        public string file_path { get; set; }
        public string file_name { get; set; }
        public int CustomerId { get; set; }
        public string OTP { get; set; }
        public List<CustomerProduct> CustomerProductList { get; set; }
        public List<CustomerActivityLog> CustomerActivityLogList { get; set; }
    }
    public class CustomerProduct : BaseClass
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public List<CustomerVariant> CustomerVariantList { get; set; }
    }
    public class CustomerVariant : BaseClass
    {
        public int VariantId { get; set; }
        public string VariantCode { get; set; }
        public string VariantName { get; set; }
        public int ProductId { get; set; }
        public int ProductTypeId { get; set; }

    }
    public class Request
    {
        public string membershipNo { get; set; }
        public string contactNo { get; set; }
        public string CustomerId { get; set; }
        public string email { get; set; }
        public string OTP_Input { get; set; }
    }
    public class Product : BaseClass
    {
        public string product_name { get; set; }
        public string product_id { get; set; }
        public string file_path { get; set; }
        public string file_name { get; set; }
        public string MemberShip { get; set; }
        public string Variant { get; set; }
        public string description { get; set; }

    }


}
