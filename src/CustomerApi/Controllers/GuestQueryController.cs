﻿using System;
using UploadEmployeeApi.Entities.Baseclasses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using UploadEmployeeApi.Business;
using UploadEmployeeApi.Entities;
using System.Collections.Generic;

namespace UploadEmployeeApi.Controllers
{
    [Route("Upload-Api/[controller]/[action]")]
    [ApiController]

    public class GuestQueryController : ControllerBase
    {
        ExcelFacade objExcelFacade = new ExcelFacade();
        ILoggerFactory _loggerFactory;
        #region insert ProductVariant
        [HttpPost]
        public ServiceResponse InsertGuestQuery(GuestQuery model)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objExcelFacade.InsertGuestQuery(model);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region GuestQuerylist
        [HttpGet]
        public List<GuestQuery> GetGuestQueryList()
        {
            List<GuestQuery> objGuestQueryList = new List<GuestQuery>();
            try
            {
                objGuestQueryList = objExcelFacade.GetGuestQueryList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objGuestQueryList;
        }
        #endregion
    }
}