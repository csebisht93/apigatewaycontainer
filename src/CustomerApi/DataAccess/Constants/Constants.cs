﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UploadEmployeeApi.DataAccess.Constants
{
    public static  class DBFields
    {

        //-----------Common Base class field ---------------------------
        public const string Id = "id";
        public const string IsActive = "is_active";
        public const string CreatedBy = "created_by";
        public const string CreatedOn = "created_on";
        public const string ModifiedBy = "modified_by";
        public const string ModifiedOn = "modified_on";
        public const string CreatedIP = "ip_address";
        public const string ModifiedIP = "modified_ip";
        public const string CreatedMacAddress = "mac_address";
        public const string ModifiedMacAddress = "modified_mac";
        public const string username = "user_name";

        public const string ErrCode = "ErrCode";


        public const string UploadId = "uploadId";
        public const string UploadBy = "uploaded_by";
        public const string FileName = "file_name";
        public const string FilePath = "file_path";
        public const string FileStatus = "file_status";

        public const string description = "description";
        public const string Variant = "Variant";
         public const string VariantCode = "variant_code";


        public const string contactno = "contact_no";
        public const string Email = "email";
       
        public const string Product = "ex_product";
        public const string address = "address";
        public const string pincode = "pincode";
        public const string Product_id = "product_id";
        public const string ProductId = "productId";
        public const string ProductName = "product_name";
        public const string ProductCode = "product_code";
        public const string VariantId = "Variant_Id";
         public const string RelativeFilePath = "RelativeFilePath";


        public const string Title = "title";
        public const string DateOfBirth = "date_of_birth";
        public const string Name = "name";
        public const string MembershipNo = "membership_no";
        public const string AlternateContactNo = "alternate_contact_no";
        public const string AlternateEmail = "alternate_email";

        public const string SearchBy = "SearchBy";
        public const string Countrycode = "Countrycode";
        public const string OTP = "OTP";
        public const string CustomerId = "CustomerId";
        public const string RequestDate = "RequestDate";

        public const string Requestid = "Request_id";
        public const string Requesttype = "Request_type";
        public const string Requestquery = "Request_query";
        public const string additional = "additional";
        public const string querystatus = "querystatus";
         public const string tRequesttype = "t_Request_type";
        



        public const string mobilenumber = "mobile_number";
        public const string msg = "msg";
        public const string status = "status";
        public const string membershipno = "membershipno";
        public const string configid = "configid";
        public const string templatecode = "templatecode";
        public const string templateId = "templateId";
        public const string OtpType = "OtpType";
        public const string Question = "question";

        public const string ExcelId = "ExcelId";
        public const string Exception = "Exception";
        public const string ErrorCode = "ErrorCode";
        public const string ExcelLine = "ExcelLine";
        public const string ExcelFile = "ExcelFile";
        public const string TotalInsert = "TotalInsert";
        public const string TotalFail = "TotalFail";
        public const string TotalEntry = "TotalEntry";
        public const string ProcessingDate = "ProcessingDate";
        public const string Filepath = "Filepath";
        public const string Successfile = "Successfile";
        public const string FailFile = "FailFile";

         public const string type = "type";

          public const string Response = "Response";

           public const string RequestFrom = "RequestFrom";






        //public static string SearchBy = "SearchBy";
        public static string Type = "type";
       // public static string FileName = "file_name";
       // public static string FilePath = "file_path";
       // public static string MemberShip = "MemberShip";
        public static string Date = "Date";
        //public static string FileStatus = "file_status";
        public static string Status = "Status";

       // public static string CustomerId = "CustomerId";
        public static string Campaigntype = "campaign_type";
        public static string Category = "category";
        public static string SubCategory = "sub_category";
        public static string EmailTo = "emailto";





    }
    public static class StoredProcedures
    {
        public const string InsertExcelUploadEntry = "sp_InsertExcelUploadRecords";
        public const string InsertZipUploadEntry = "sp_InsertZipUploadRecords";
        public const string InsertCustomer = "sp_InsertCustomer";

        public const string PdfMemberShipSearch = "sp_PdfMemberShipSearch"; 
        public const string updatePdfMemberShip = "sp_updatePdfMemberShip";

          public const string GetCustomerList = "sp_GetCustomerList";
        public const string GetCustomerDetailsById = "sp_GetCustomerDetailsById";
            public const string CustomerVariantList = "sp_CustomerVariantList";
           public const string GetCustomerDetailsByMemberID = "sp_GetCustomerDetailsByMemberID";

               public const string GetExcelUploadTrackerList = "sp_GetExcelUploadTrackerList";

        public const string InsertUpdateCustomer = "sp_InsertUpdateCustomer";
        public const string GetZipUploadTrackerData = "sp_GetZipUploadTrackerData"; 
        public const string SearchByPolicyEmailMobile = "sp_SearchByPolicyEmailMobile";
        public const string SaveCustomerOTP = "sp_SaveCustomerOTP";

        public const string CheckCustomerOTPGetData = "sp_CheckCustomerOTPGetData";
         public const string GetCustomerProduct = "sp_GetCustomerProduct";

        public const string GetCustomerListForSupport = "sp_GetCustomerListForSupport";
          public const string CustomerAutoCompleteSearch = "sp_CustomerAutoCompleteSearch";
        public const string ValidateCustomerOTP = "sp_ValidateCustomerOTP"; 
        public const string GetCustomerQueries = "sp_GetCustomerQuery";

        //DB change
        public const string InsertOTPEntries = "sp_InsertOTPEntries";
        public const string SaveCustomerRequest = "sp_saveCustomerRequest";
      //  public const string SaveCustomerProfileRequest = "sp_saveCustomerProfileRequest";
        //

        public const string InsertGuestQuery = "sp_InsertGuestQuery";
        public const string GetGuestQueryList = "sp_GetGuestQueryList";

        public const string GetExcelInfoListByExcelId = "sp_GetExcelInfoListByExcelId";
    public const string InsertCustomerActivityLog = "sp_InsertCustomerActivityLog";
      public const string GetCustomerActivityLogList = "sp_GetCustomerActivityLogList";

        public static string GetCustomerDocUploadReport = "proc_GetCustomerDocumentUploadReport";
        public static string GetCustomeEmailReport = "Proc_GetCustomerEmailReport";
      public static string GetCustomeSMSReport = "Proc_GetCustomeSMSReport";
    }
}
